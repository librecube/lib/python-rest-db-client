# API Documentation

## Connect

The first step is to create a client to the RESTful database:

```python
from rest_db_client import RestDbClient
client = RestDbClient(port=6501)
```

By default it assumes that the database is available at http://localhost.
If not, then provide the host name and the port, or an URL.

The RESTful database is expected to organize its data in terms of , data is
organized as domains and models. To access the data of domains and models,
like so:

```python
domain = client['Galaxy']
model = domain['SolarSystem']
```

## Insert Data

To insert data (in the form of datasets) into the model, do like so:

```python
datasets = [
  {
    'planet': 'Jupiter',
    'moons': 69
  },
  {
    'planet': 'Mars'
  },
  {
    'planet': 'Earth',
    'moons': 1,
    'inhabited': True
  },
]
model.insert(datasets)
```

## Read Data

To read all datasets from a model, issue:

```python
datasets = model.find()
```

To apply filtering and other query options, pass these in the method call. For example, get the names of all planets with that have at least one moon, sorted by number of moons in descending order:

```python
datasets = model.find(
    filter={'moons': {'ge': 1}},
    fields=['planet'],
    sort=['-moons']
)
```

## Delete Data

The deletion of datasets works in similar way as querying. For example, to delete all planets with more than 10 moons from the database, issue:

```python
model.delete(filter={'moons': {'gt': 10}})
```

To delete the entire model, issue either of the following:

```python
model.delete()
domain.drop(model.name)
domain.drop('SolarSystem')
```
